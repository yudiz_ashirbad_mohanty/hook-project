import React, {useRef ,useState } from 'react'
import JokemakerTwo from './JokemakerTwo'
import './jokeone.css'
const JokermakerOne = () => {
    const firstNameRef =useRef(null);
    const lastNameRef =useRef(null);
    const [fastname,setfirstname]=useState("FastName");
    const [lastname,setlastname]=useState("");
    const joke =JokemakerTwo(fastname,lastname);
    const generatorjoke = (e)=>{
        e.preventDefault();
        setfirstname(firstNameRef.current.value);
        setlastname(lastNameRef.current.value);
    }
   
  return (
    <div className='joke'>
            <h2>
                The JOKE MAKER
            </h2>
            <h3>{joke}</h3>
            <form>
                <input placeholder='Fast Name' ref={firstNameRef}/>
                <input placeholder='Last Name' ref={lastNameRef }/>
                <button onClick={generatorjoke}>joke maker</button>
            </form>
    </div>
  )
}

export default JokermakerOne