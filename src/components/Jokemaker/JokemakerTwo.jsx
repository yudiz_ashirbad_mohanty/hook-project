import {useState,useEffect} from 'react'

const JokemakerTwo = (fastname,Lastname) => {
    const [joke,setjoke]=useState("");
   
    useEffect(()=>{
    const fetchurl =async ()=>{
        await fetch(
            `http://api.icndb.com/jokes/random?firstName= ${fastname}&lastName=${Lastname}`
        ).then(response =>response.json()).then(data=>{
            setjoke(data.value.joke)
        })       
        }
        fetchurl();
    },[fastname,Lastname])
  return (
    joke
  )
}

export default JokemakerTwo