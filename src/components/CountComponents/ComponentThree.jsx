import React,{useContext} from 'react'
import {Countcontext } from '../../App'
import './countthree.css'

const ComponentThree = () => {
    const countcontext =useContext(Countcontext)
  return (
    <div className='component3'>
     COUNT = {countcontext.countState}
     <br/>
    <button onClick={()=>countcontext.countDispact('increment')}>
    Increment
    </button>
    <button onClick={()=>countcontext.countDispact('Decrement')}> Decrement
    </button>
    <button onClick={()=>countcontext.countDispact('reset')}> RESET
    </button>
    </div>
  )
}

export default ComponentThree