import React, { useContext } from 'react'
import {Countcontext } from '../../App'
import './countone.css'
const ComponentOne = () => {
    const countcontext =useContext(Countcontext)
  return (
    <div className='component1'>
    COUNT = {countcontext.countState}
    <br/>
    <button onClick={()=>countcontext.countDispact('increment')}> Increment
    </button>
    <button onClick={()=>countcontext.countDispact('Decrement')}> Decrement
    </button>
    <button onClick={()=>countcontext.countDispact('reset')}> RESET
    </button>
    </div>
  )
}

export default ComponentOne