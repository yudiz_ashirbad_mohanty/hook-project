import React,{useContext} from 'react'
import {Countcontext } from '../../App'
import './counttwo.css'
const ComponentTwo = () => {
    const countcontext =useContext(Countcontext)
  return (
    <div className='component2'>
    COUNT = {countcontext.countState}
    <br/>
    <button onClick={()=>countcontext.countDispact('increment')}>
    Increment
    </button>
    <button onClick={()=>countcontext.countDispact('Decrement')}> Decrement
    </button>
    <button onClick={()=>countcontext.countDispact('reset')}> RESET
    </button>
    </div>
  )
}

export default ComponentTwo