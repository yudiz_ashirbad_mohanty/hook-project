import React,{useReducer} from 'react'
import './App.css';
import ComponentOne from './components/CountComponents/ComponentOne';
import ComponentThree from './components/CountComponents/ComponentThree';

import ComponentTwo from './components/CountComponents/ComponentTwo';

import JokermakerOne from './components/Jokemaker/JokermakerOne';

export const Countcontext =React.createContext()
const initialstate =0;
const reducer =(state,action)=>{
  switch(action){
    case 'increment':
      return state +1
      case 'Decrement':
        return state -1
        case 'reset':
          return initialstate
          default:
            return state
  }
}


function App() {
 const [count,dispatch]=useReducer(reducer,initialstate)
  return (
   <>
   <Countcontext.Provider value={{countState:count,countDispact:dispatch}}>
   <div className='App'>
   <div className='subApp'>
   <ComponentOne/>
   <br/>
   <ComponentTwo/>
   <br/>
   <ComponentThree/>
   </div>
   </div>
   </Countcontext.Provider>
   <JokermakerOne/>
   </>
  );
}

export default App;


